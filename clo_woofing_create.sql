DROP DATABASE IF EXISTS `woofing`;
CREATE DATABASE `woofing` DEFAULT CHARACTER SET utf8 ;
USE `woofing` ;

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE `user` (
  `user_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `email` VARCHAR(45) NOT NULL UNIQUE ,
  `password` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45),
  `username` VARCHAR(16) NOT NULL,
  `is_host` TINYINT NOT NULL,
  
  PRIMARY KEY (`user_id`)
  );


-- -----------------------------------------------------
-- Table `host_place`
-- -----------------------------------------------------
CREATE TABLE `host_place` (
  `host_place_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `adress` VARCHAR(255) NOT NULL,
  `zipcode` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `capacity` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `host_id` INT NOT NULL,
  
  PRIMARY KEY (`host_place_id`),
  
  CONSTRAINT `fk_host_place__host`
    FOREIGN KEY (`host_id`)
    REFERENCES `user` (`user_id`)
    );


-- -----------------------------------------------------
-- Table `skill`
-- -----------------------------------------------------
CREATE TABLE `skill` (
  `skill_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `name` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  
  PRIMARY KEY (`skill_id`)
  );


-- -----------------------------------------------------
-- Table `project`
-- -----------------------------------------------------
CREATE TABLE `project` (
  `project_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `host_place_id` INT NOT NULL,
  
  PRIMARY KEY (`project_id`),
  
  CONSTRAINT `fk_project__host_place`
    FOREIGN KEY (`host_place_id`)
    REFERENCES `host_place` (`host_place_id`)
    );


-- -----------------------------------------------------
-- Table `host_place_has_skill`
-- -----------------------------------------------------
CREATE TABLE `host_place_has_skill` (
  `host_place_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  
  CONSTRAINT `fk_host_place_has_skill__host_place`
    FOREIGN KEY (`host_place_id`)
    REFERENCES `host_place` (`host_place_id`),
    
  CONSTRAINT `fk_host_place_has_skill__skill`
    FOREIGN KEY (`skill_id`)
    REFERENCES `skill` (`skill_id`)
    );


-- -----------------------------------------------------
-- Table `stay`
-- -----------------------------------------------------
CREATE TABLE `stay` (
  `stay_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `arrival` DATE NULL,
  `departure` DATE NULL,
  `volonteer_id` INT NOT NULL,
  `project_id` INT,
  `host_place_id` INT,
  
  PRIMARY KEY (`stay_id`),
  
  CONSTRAINT `fk_stay__volonteer`
    FOREIGN KEY (`volonteer_id`)
    REFERENCES `user` (`user_id`),
    
  CONSTRAINT `fk_stay__project`
    FOREIGN KEY (`project_id`)
    REFERENCES `project` (`project_id`),
    
  CONSTRAINT `fk_stay__host_place`
    FOREIGN KEY (`host_place_id`)
    REFERENCES `host_place` (`host_place_id`)
    );


-- -----------------------------------------------------
-- Table `user_has_skill`
-- -----------------------------------------------------
CREATE TABLE `user_has_skill` (
  `user_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  `level` INT NULL,
  
  CONSTRAINT `fk_user_has_skill__user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`),
    
  CONSTRAINT `fk_user_has_skill__skill`
    FOREIGN KEY (`skill_id`)
    REFERENCES `skill` (`skill_id`)
    );


-- -----------------------------------------------------
-- Table project_has_skill`
-- -----------------------------------------------------
CREATE TABLE `project_has_skill` (
  `project_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  
  CONSTRAINT `fk_project_has_skill__project`
    FOREIGN KEY (`project_id`)
    REFERENCES `project` (`project_id`),
    
  CONSTRAINT `fk_project_has_skill__skill`
    FOREIGN KEY (`skill_id`)
    REFERENCES `skill` (`skill_id`)
    );


-- -----------------------------------------------------
-- Table `message`
-- -----------------------------------------------------
CREATE TABLE `message` (
  `message_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `message_content` TEXT NOT NULL,
  `date` DATETIME NOT NULL,
  `sender_id` INT NOT NULL,
  `recipient_id` INT NOT NULL,
  
  PRIMARY KEY (`message_id`),
  
  CONSTRAINT `fk_message__sender`
    FOREIGN KEY (`sender_id`)
    REFERENCES `user` (`user_id`),
    
  CONSTRAINT `fk_message__recipient`
    FOREIGN KEY (`recipient_id`)
    REFERENCES `user` (`user_id`)
    );


-- -----------------------------------------------------
-- Table `log`
-- -----------------------------------------------------
CREATE TABLE `log` (
  `log_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `content` TEXT NOT NULL,
  `date` DATETIME NOT NULL
  
  PRIMARY KEY (`log_id`)
  );
