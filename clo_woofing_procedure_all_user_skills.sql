USE `woofing`;
DROP procedure IF EXISTS `all_user_skill`;

DELIMITER $$
USE `woofing`$$
CREATE PROCEDURE `all_user_skill` (id_user INT)
BEGIN
	SELECT sk.name FROM skill sk INNER JOIN user_has_skill us ON us.skill_id = sk.skill_id INNER JOIN user u ON u.user_id = us.user_id WHERE u.user_id = id_user
UNION
SELECT sk.name FROM skill sk 
	INNER JOIN project_has_skill ps ON ps.skill_id = sk.skill_id
    INNER JOIN stay st ON st.project_id = ps.project_id
    INNER JOIN user u ON u.user_id = st.volonteer_id 
	WHERE u.user_id = id_user
UNION
SELECT sk.name FROM skill sk 
	INNER JOIN host_place_has_skill hps ON hps.skill_id = sk.skill_id
    INNER JOIN stay st ON st.host_place_id = hps.host_place_id
    INNER JOIN user u ON u.user_id = st.volonteer_id 
	WHERE u.user_id = id_user;
END$$

DELIMITER ;
