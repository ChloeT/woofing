USE `woofing`;

INSERT INTO `user_has_skill` (`user_id`, `skill_id`, `level`) 
VALUES 
    (11,7,4),
    (76,17,3),
    (45,16,5),
    (1,1,3),
    (36,15,3),
    (40,14,4),
    (41,6,5),
    (81,11,5),
    (53,17,3),
    (33,9,3),
    (82,2,1),
    (54,19,5),
    (7,7,1),
    (89,19,5),
    (69,8,1),
    (23,12,3),
    (59,1,3),
    (26,9,1),
    (61,14,2),
    (67,12,3),
    (31,10,3),
    (49,9,4),
    (21,5,4),
    (69,16,3),
    (12,9,2),
    (79,11,3),
    (27,2,4),
    (53,7,1),
    (9,2,3),
    (39,3,1),
    (89,10,1),
    (52,6,1),
    (45,5,2),
    (18,20,4),
    (53,6,3),
    (93,17,3),
    (86,6,2),
    (16,15,4),
    (38,11,1),
    (2,12,3),
    (15,8,4),
    (44,19,5),
    (19,14,2),
    (4,11,4),
    (65,20,1),
    (95,12,1),
    (37,13,2),
    (66,5,3),
    (1,19,1),
    (73,9,1),
    (59,3,1),
    (93,2,1),
    (26,4,2),
    (9,16,3),
    (7,17,1),
    (92,15,2),
    (1,14,3),
    (35,3,4),
    (17,1,5),
    (68,1,5),
    (84,2,3),
    (35,4,4),
    (61,1,4),
    (71,1,1),
    (40,11,3),
    (38,1,2),
    (87,1,1),
    (53,1,3),
    (50,1,2),
    (66,11,4),
    (36,13,1),
    (20,8,2),
    (100,2,3),
    (89,13,5),
    (51,2,4),
    (90,14,3),
    (68,7,2),
    (82,7,5),
    (41,2,5),
    (11,19,2),
    (13,16,2),
    (84,6,5),
    (21,18,2),
    (32,10,1),
    (40,17,4),
    (42,19,3),
    (46,15,5),
    (49,2,1),
    (17,18,1),
    (8,12,1),
    (16,6,4),
    (96,12,1),
    (38,13,4),
    (14,9,2),
    (33,19,4),
    (73,11,1),
    (71,15,1),
    (45,14,5),
    (90,15,5),
    (74,10,5);
