USE `woofing`;
DROP procedure IF EXISTS `hosting_type_percent`;

DELIMITER $$
USE `woofing`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `hosting_type_percent`(hosting_type VARCHAR(45))
BEGIN
	DECLARE total_place INT;
    DECLARE this_type_place INT;
    
	SELECT count(*) INTO total_place FROM host_place;
    SELECT count(*) INTO this_type_place FROM host_place WHERE type = hosting_type;
    
    SELECT this_type_place * 100 / total_place AS hosting_type_percent, hosting_type AS hosting; 
    
END$$

DELIMITER ;
