USE `woofing`;
DROP procedure IF EXISTS `remaining_place_at_date`;

DELIMITER $$
USE `woofing`$$
CREATE PROCEDURE `remaining_place_at_date` (id_host_place INT, date DATE)
BEGIN
    DECLARE place_capacity INT;
    DECLARE place_taken INT;

    SELECT capacity INTO place_capacity FROM host_place WHERE host_place = id_host_place;
    SELECT count(*) INTO place_taken FROM stay  WHERE (
            host_place_id = id_host_place 
            OR project_id IN (SELECT project_id FROM project WHERE host_place_id = id_host_place)
        ) AND (
            from_date <= date 
            AND date <= to_date
        );

    SELECT place_capacity - place_taken AS remaining_place;
END$$

DELIMITER ;
