USE `woofing`;

INSERT INTO `user` (`email`, `password`, `salt`, `username`, `is_host`) 
VALUES
	("chadd.herzog@example.com",		"cbf71bd25bc57c5c8b6432b9cad709af", 	"hhkc", 	"uhand", 			0),
	("ijacobi@example.net", 			"1ae2cf2ce9de542a3dc504c2b3bb79b1", 	"woyd", 	"roger75", 			1),
	("mwaters@example.com", 			"71573820cd5a24426a11f5834c152aa9", 	"szal", 	"littel.cydney", 	0),
	("qklocko@example.org", 			"255b6acdfe51828fcb230ceed819b3f6", 	"ccnm", 	"mara.ullrich", 	0),
	("stanton.stanley@example.net", 	"821c79aba4e98e72006191571f8fe077", 	"kelr", 	"ebert.kaylin", 	0),
	("cheyanne57@example.net", 			"9c2a4f404d26ce422b851d2ea87fb410", 	"zcru", 	"ayana.okuneva",	0),
	("samir.bode@example.com", 			"9e83c419b4b40b56ac971ee3fad3773f", 	"zgcl", 	"cheyanne.carter", 	0),
	("deshaun.olson@example.org", 		"4795f35574ee2b81df4439e9058de8d6", 	"yxir", 	"flo.sawayn", 		1),
	("pschinner@example.org", 			"3f573bd08564b3811ec96b35f99c8a30", 	"xmmw", 	"rsatterfield", 	0),
	("bode.joan@example.com", 			"11b4975e395e93d8140dfc334a86b007", 	"uexm", 	"devante73", 		0),
	("vcartwright@example.com", 		"073d5fedbb62d1430c5af8c28b41a453", 	"xbfm", 	"afranecki",		1),
	("lockman.eliza@example.net", 		"0c5fae7de2932c952783aa6055da49f7", 	"miwh", 	"runolfsdottir.mc", 1),
	("janice.fritsch@example.org", 		"04d0a7a67cd69b7f49fdf139c29368ba", 	"duih", 	"heathcote.amiya", 	0),
	("iprosacco@example.com", 			"bb56e26a3ac9a3faafb099c8f45d4103", 	"stou", 	"geraldine66", 		0),
	("perdman@example.com", 			"204876894f9a8e4a769b0ae80ef06856", 	"rlgf", 	"lesch.kevon", 		0),
	("marlen.collins@example.net", 		"d8d3eb031d90e96739b9730b5d12ca9e", 	"pvxp", 	"kkonopelski", 		0),
	("broderick.hills@example.com", 	"55e36e8d261c5cc3e07c3214d19d4271", 	"bzpe", 	"khalil.sipes", 	1),
	("conn.destiney@example.com", 		"a19bc511fac5af8cdfff00451c906cd4", 	"byim", 	"eden81", 			1),
	("schiller.raphael@example.net", 	"7d1c7550e602a7bc56afbe501218cf31", 	"tcnh", 	"kluettgen", 		1),
	("dorris64@example.org", 			"b127782ef8d9ec2efacf32a181160ec8", 	"xryn", 	"kelvin37", 		1),
	("beryl.funk@example.net", 			"7b485a44a23319745e63d677d7c8f617", 	"rpkd", 	"bernadine.kuhn", 	1),
	("emmerich.marie@example.net", 		"e05abc7a4e25f0dd37d6e177eb0b03f3", 	"shay", 	"marques50", 		1),
	("sonia.streich@example.com", 		"982df4a953ee030dcd6d73e848f5efb4", 	"xdpz", 	"egutmann", 		1),
	("maximus40@example.org", 			"3cfcb942cfe193f76d7b68373d8517eb", 	"mest", 	"durgan.madeline", 	0),
	("cletus.kohler@example.org", 		"068bd4bd2d95e6c6ff108bc1ec384761", 	"bzwm", 	"magali.ortiz", 	0),
	("kiel98@example.net", 				"ba2e082274e6ff5aea2b70cf0e9e407d", 	"qtzf", 	"haag.sigurd", 		0),
	("sidney.klocko@example.org", 		"f8c9dab55b7148a560c319cd2514c82a", 	"dztt", 	"thickle", 			1),
	("reese78@example.com",				"503d476e8d46a5504ce4eece1d0f5294", 	"mcnp", 	"parker.jaqueline", 0),
	("laurence.kuhlman@example.net", 	"5724c7bebd51cd392f042f0e8a0a6139", 	"qmsv", 	"brosenbaum", 		0),
	("osmith@example.net", 				"d3138fef06a66479e89dbd32943be6dc", 	"evde", 	"bernier.myra", 	1),
	("myrl.thiel@example.net", 			"31e428d718b4ff3ba87a248dff468330", 	"rsfi", 	"stark.ubaldo", 	0),
	("yazmin.ritchie@example.net", 		"60093363dd34f162cd4a283eb56996c0", 	"yvku", 	"chamill", 			0),
	("khalil25@example.com", 			"88adcc9d87f1cade0dbc9dd0d435ee9a", 	"sbgb", 	"karolann.crona", 	1),
	("homenick.francis@example.com", 	"72a81d127f35691344e8ebc1e45ffefa", 	"egcv", 	"west.baby", 		0),
	("elijah.von@example.net", 			"0d12b0b3355afe86a6c58afc469fa67c", 	"ypjb", 	"ryleigh.schultz", 	0),
	("uroob@example.com", 				"a55d37e2b478ec4dba663aabab974672", 	"tmld", 	"dietrich.brad", 	1),
	("sritchie@example.org", 			"cc21d672de50b7506d8d5b159557c583", 	"pqtp", 	"alisa.ankunding", 	0),
	("nschamberger@example.com", 		"dd79876951716dd88e83f553feaf5dec", 	"ochk", 	"weissnat.tiara", 	1),
	("lakin.vella@example.net", 		"152cedf13052a4f20cf752b7adaec790", 	"zxzv", 	"sim59", 			0),
	("cschuster@example.org", 			"dddfe569395a90a72dace61b2d5324af", 	"hopw", 	"eldora.gaylord", 	1),
	("yasmin45@example.net", 			"f16e261df951de8087a24066a4ffea15", 	"hfkz", 	"lottie71", 		1),
	("bruce.moore@example.com", 		"ba51068cd4ff9b7cbed6a3d83ab0152a", 	"peoj", 	"vivien96", 		0),
	("angelica32@example.net",			"da607b8ce19e2413cc8b5779c37d9dfd", 	"hovx", 	"omosciski", 		0),
	("precious.harvey@example.org", 	"e1f147a680f4a7165ca9286fdab965e3", 	"oipg", 	"mpowlowski", 		0),
	("moriah80@example.org", 			"7f14f5460654e7572cadf5de451df145", 	"dykb", 	"mccullough.shyan", 1),
	("kristin19@example.org", 			"ee2bfaad2066e278fd784bc947769d8b", 	"jkio", 	"jay.heller", 		1),
	("jacey97@example.org", 			"7c080a5ea38d03bab503c9f95dd7b89b", 	"yprj", 	"eveline.bartell", 	0),
	("stiedemann.kennedy@example.com", 	"9a577e0cef802c1719b7998d4b80114a", 	"topo", 	"srodriguez", 		0),
	("eve.wintheiser@example.org", 		"b8e9f2aa89bf583dce6cb28029730c56", 	"lswx", 	"hazel.koss", 		0),
	("crona.kacey@example.org", 		"745cdf6c93562c39f97e2e7c78573da7", 	"mnwn", 	"lhackett", 		1),
	("bkub@example.com", 				"691bed7336bf848db39dd3455dd1e9ca", 	"kuhf", 	"littel.teresa", 	0),
	("makayla.casper@example.com", 		"10bfdf3d5e191a9e1308b4f641b772da", 	"ypdw", 	"murphy.creola", 	1),
	("valentin75@example.org", 			"618ace4687399fca7c3c7c3043ea5bd1", 	"masm", 	"hoppe.irma", 		0),
	("neva.hilpert@example.net", 		"12b6d70bad821d59d5053f3b6860fa66", 	"sqfh", 	"helen.parisian", 	1),
	("bonnie42@example.com", 			"d344753a19f27175747a6a609108c22d", 	"devu", 	"adam47", 			0),
	("lamont59@example.org", 			"c500ee9b9a57e7bf647b616734eaef02", 	"xkbw", 	"iwelch", 			1),
	("kparker@example.org",				"89aea689c6db66f16b042ee92060df86", 	"sbkd", 	"becker.manley", 	1),
	("lhyatt@example.net", 				"9e4b378eab6c872ba2408a425ee498a0", 	"nnfm", 	"cernser", 			0),
	("kiehn.jevon@example.com", 		"1695287658637ef845c6de7dbe64e088", 	"avcg", 	"vjacobson", 		1),
	("efarrell@example.net", 			"e15dec6b98ed9cc5b4960c014756b20b", 	"hyln", 	"curtis.hintz", 	0),
	("kbraun@example.org", 				"af3c3fcc2f6b3a3bff28e32d92a7cb5c", 	"walp", 	"ehilll", 			1),
	("aschaefer@example.com", 			"b0f3c89603231363c566e70ea602f643", 	"xzcm", 	"jessie40", 		0),
	("estelle48@example.com", 			"ae0e643c3539411b5e331f1afad17057", 	"itxp", 	"lblock", 			1),
	("sarai.smith@example.net", 		"c63125ef3342462895ca2cd76471ef35", 	"uqyk", 	"kertzmann.janet", 	0),
	("jcrona@example.com", 				"534dde4ec9eb76b954c25961d0562125", 	"fauv", 	"hillary81", 		1),
	("blake31@example.org", 			"2156a41dd46bf477eaea706c0285b77e", 	"ysch", 	"fterry", 			0),
	("simonis.breana@example.com", 		"e955e50b61add5312b9ee2d42c121438", 	"icij", 	"jaskolski.gene", 	1),
	("spouros@example.org", 			"8e419db99840a8b33204c8919bd53355", 	"lkxk", 	"mlindgren", 		0),
	("mortimer31@example.com", 			"b9904f879594278cf806a541513d484f", 	"bciq", 	"hbogisich", 		0),
	("donald.bayer@example.net", 		"a6b84331b026e4fcabbff96641a416b6", 	"jpty", 	"rex42", 			1),
	("gwiza@example.net", 				"ed2bbcaa8c35d7846f1f4b8b4cf490bd", 	"kytr", 	"alfred.weimann", 	1),
	("skyla.okon@example.org", 			"e7872ba0677b7832b865e6668b19e88d", 	"meak", 	"renee.heller", 	1),
	("brett41@example.org", 			"51474f79b1b544598fcb820d55fe440f", 	"mdbx", 	"klocko.tommie", 	0),
	("king.dolly@example.net", 			"f8f64c2ea0a6b211ac537046f3f4763b", 	"fqsd", 	"griffin.welch", 	1),
	("batz.vickie@example.com", 		"93b8bf674bcbc9a62215f4929e7593c3", 	"axja", 	"cameron23", 		0),
	("jared99@example.net", 			"e3b91da3c8296fe0dd8ccc0e3b089404", 	"lsql", 	"mckayla76", 		1),
	("jswift@example.net", 				"846b9e6603a6b479a10da713f55e5b79", 	"khob", 	"hkessler", 		1),
	("schmitt.pedro@example.com", 		"854a798ab73a1676b13760c00d402c59", 	"lxqh", 	"iwalter", 			1),
	("carson02@example.net", 			"63b1cea6446d6683a97019718eefa637", 	"aqlv", 	"senger.winona", 	0),
	("brayan26@example.org", 			"aa0cfcb11a8311a019623c2f9cf6642b", 	"gebj", 	"florian58", 		1),
	("nwill@example.net", 				"aec9ebf6272b00a57fe8c53785941dd6", 	"rrny", 	"lue.towne", 		0),
	("hirthe.eden@example.org", 		"edf68bf4e611c69941d839341e34ec9b", 	"mmtq", 	"qspinka", 			1),
	("brent.toy@example.com", 			"85871a7b54e281caa4a7b24647db548c", 	"ihlk", 	"kyle68", 			1),
	("ffeest@example.com", 				"3caf0b5088234d9ec9cdcaec1169fdcf", 	"yhpl", 	"o'conner.jaron", 	0),
	("leone17@example.net", 			"e4af008512642c2c605373d456c2e9aa", 	"pqqr", 	"estrella.abshire", 0),
	("lupton@example.com", 				"e266817876fcd9b81f041b87ac1ec376", 	"hstb", 	"progahn", 			1),
	("mertz.antoinette@example.net", 	"e30b2388153f5d234f2be0f3556a6599", 	"bczk", 	"wprosacco", 		1),
	("jaiden70@example.org", 			"65c4dd36b4e0e5ee51afb3cbe206425f", 	"vqmg", 	"nelda.rowe", 		0),
	("nayeli08@example.com", 			"798b7d6ba9d418c26c98619d0b8be943", 	"qzfn", 	"xhammes", 			1),
	("buford08@example.com", 			"06e2dc75b9076913e79b4c79506ef0b5", 	"oqim", 	"jared61", 			1),
	("zvonrueden@example.org", 			"7487abb2c1e5dfaae5b72059510ac81d", 	"szzb", 	"nbergnaum", 		0),
	("gbatz@example.com", 				"551fcf24a46f059862bf7aa4fd784cda", 	"rgmz", 	"kshlerin.dessie", 	0),
	("hgaylord@example.org", 			"11bf887be5d3a7a98a697304d9b96010", 	"eogq", 	"boyle.eldred", 	1),
	("rebekah67@example.org", 			"a46a1510e979a516c3c9513576fb7927", 	"zbbi", 	"xlesch", 			0),
	("ullrich.herminia@example.net", 	"51909bf9d064966ea91e527cc0c49525", 	"tmcb", 	"ymorissette", 		1),
	("sydney.green@example.org", 		"c7b302da2093332da133279dfa9e79f5", 	"oafx", 	"juliet80", 		0),
	("elenora94@example.com", 			"ef7a1534a695aef104ed7303cdf38c48", 	"svpq", 	"colton.hauck", 	0),
	("loyce.harvey@example.net", 		"c382fe8051836ec95044dc3373a9c592", 	"etbf", 	"pacocha.roger", 	1),
	("stamm.aniya@example.net", 		"2b1a1122bd6896b07710e44f479a0358", 	"tqix", 	"eriberto36", 		1),
	("	modesto.hauck@example.org", 	"b4010c84f1c8d5fcd6a1cd8b4427532c", 	"dtap", 	"lou.von", 			0);