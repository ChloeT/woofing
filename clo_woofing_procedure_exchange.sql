USE `woofing`;
DROP procedure IF EXISTS `find_exchange`;

DELIMITER $$
USE `woofing`$$
CREATE PROCEDURE `find_exchange` (id_first_user INT, id_second_user INT)
BEGIN
	SELECT m.* FROM message m WHERE (
			sender_id = id_first_user 
			AND
            recipient_id = id_second_user
		)OR(
			sender_id = id_second_user 
			AND
            recipient_id = id_first_user
		) ORDER BY date;
END$$

DELIMITER ;
