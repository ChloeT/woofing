DROP TRIGGER IF EXISTS `woofing`.`stay_AFTER_INSERT`;

DELIMITER $$
USE `woofing`$$
CREATE DEFINER=`simplon`@`localhost` TRIGGER `woofing`.`stay_AFTER_INSERT` AFTER INSERT ON `stay` FOR EACH ROW
BEGIN
	INSERT INTO `log`(content, date)
	VALUE (concat("new stay registered : id :", new.stay_id),NOW());
END$$
DELIMITER ;
