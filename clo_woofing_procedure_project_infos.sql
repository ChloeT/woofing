USE `woofing`;
DROP procedure IF EXISTS `project_infos`;

DELIMITER $$
USE `woofing`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `project_infos`(id_project INT)
BEGIN
	SELECT p.from_date, p.to_date, h.username, hp.adress, hp.zipcode, hp.city, hp.country, hp.capacity, s.name AS skill FROM skill s 
		INNER JOIN project_has_skill ps ON ps.skill_id = s.skill_id
        INNER JOIN project p ON p.project_id = ps.project_id
        INNER JOIN host_place hp ON p.host_place_id = hp.host_place_id
        INNER JOIN user h ON hp.host_id = h.user_id
        WHERE p.project_id = id_project ;
END$$

DELIMITER ;
