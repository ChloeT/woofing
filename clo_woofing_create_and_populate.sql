DROP DATABASE IF EXISTS `woofing`;
CREATE DATABASE `woofing` DEFAULT CHARACTER SET utf8 ;
USE `woofing` ;

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE `user` (
  `user_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `email` VARCHAR(45) NOT NULL UNIQUE ,
  `password` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45),
  `username` VARCHAR(16) NOT NULL,
  `is_host` TINYINT NOT NULL,
  
  PRIMARY KEY (`user_id`)
  );


-- -----------------------------------------------------
-- Table `host_place`
-- -----------------------------------------------------
CREATE TABLE `host_place` (
  `host_place_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `adress` VARCHAR(255) NOT NULL,
  `zipcode` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `capacity` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `host_id` INT NOT NULL,
  
  PRIMARY KEY (`host_place_id`),
  
  CONSTRAINT `fk_host_place__host`
    FOREIGN KEY (`host_id`)
    REFERENCES `user` (`user_id`)
    );


-- -----------------------------------------------------
-- Table `skill`
-- -----------------------------------------------------
CREATE TABLE `skill` (
  `skill_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `name` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  
  PRIMARY KEY (`skill_id`)
  );


-- -----------------------------------------------------
-- Table `project`
-- -----------------------------------------------------
CREATE TABLE `project` (
  `project_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `host_place_id` INT NOT NULL,
  
  PRIMARY KEY (`project_id`),
  
  CONSTRAINT `fk_project__host_place`
    FOREIGN KEY (`host_place_id`)
    REFERENCES `host_place` (`host_place_id`)
    );


-- -----------------------------------------------------
-- Table `host_place_has_skill`
-- -----------------------------------------------------
CREATE TABLE `host_place_has_skill` (
  `host_place_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  
  CONSTRAINT `fk_host_place_has_skill__host_place`
    FOREIGN KEY (`host_place_id`)
    REFERENCES `host_place` (`host_place_id`),
    
  CONSTRAINT `fk_host_place_has_skill__skill`
    FOREIGN KEY (`skill_id`)
    REFERENCES `skill` (`skill_id`)
    );


-- -----------------------------------------------------
-- Table `stay`
-- -----------------------------------------------------
CREATE TABLE `stay` (
  `stay_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `arrival` DATE NULL,
  `departure` DATE NULL,
  `volonteer_id` INT NOT NULL,
  `project_id` INT,
  `host_place_id` INT,
  
  PRIMARY KEY (`stay_id`),
  
  CONSTRAINT `fk_stay__volonteer`
    FOREIGN KEY (`volonteer_id`)
    REFERENCES `user` (`user_id`),
    
  CONSTRAINT `fk_stay__project`
    FOREIGN KEY (`project_id`)
    REFERENCES `project` (`project_id`),
    
  CONSTRAINT `fk_stay__host_place`
    FOREIGN KEY (`host_place_id`)
    REFERENCES `host_place` (`host_place_id`)
    );


-- -----------------------------------------------------
-- Table `user_has_skill`
-- -----------------------------------------------------
CREATE TABLE `user_has_skill` (
  `user_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  `level` INT NULL,
  
  CONSTRAINT `fk_user_has_skill__user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`),
    
  CONSTRAINT `fk_user_has_skill__skill`
    FOREIGN KEY (`skill_id`)
    REFERENCES `skill` (`skill_id`)
    );


-- -----------------------------------------------------
-- Table project_has_skill`
-- -----------------------------------------------------
CREATE TABLE `project_has_skill` (
  `project_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  
  CONSTRAINT `fk_project_has_skill__project`
    FOREIGN KEY (`project_id`)
    REFERENCES `project` (`project_id`),
    
  CONSTRAINT `fk_project_has_skill__skill`
    FOREIGN KEY (`skill_id`)
    REFERENCES `skill` (`skill_id`)
    );


-- -----------------------------------------------------
-- Table `message`
-- -----------------------------------------------------
CREATE TABLE `message` (
  `message_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `message_content` TEXT NOT NULL,
  `date` DATETIME NOT NULL,
  `sender_id` INT NOT NULL,
  `recipient_id` INT NOT NULL,
  
  PRIMARY KEY (`message_id`),
  
  CONSTRAINT `fk_message__sender`
    FOREIGN KEY (`sender_id`)
    REFERENCES `user` (`user_id`),
    
  CONSTRAINT `fk_message__recipient`
    FOREIGN KEY (`recipient_id`)
    REFERENCES `user` (`user_id`)
    );


-- -----------------------------------------------------
-- Table `log`
-- -----------------------------------------------------
CREATE TABLE `log` (
  `log_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `content` TEXT NOT NULL,
  `date` DATETIME NOT NULL
  
  PRIMARY KEY (`log_id`)
  );



-- -----------------------------------------------------
-- -----------------------------------------------------
-- Populate tables
-- -----------------------------------------------------
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Populate table `user`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `user` (`email`, `password`, `salt`, `username`, `is_host`) 
VALUES
	("chadd.herzog@example.com",		"cbf71bd25bc57c5c8b6432b9cad709af", 	"hhkc", 	"uhand", 			0),
	("ijacobi@example.net", 			"1ae2cf2ce9de542a3dc504c2b3bb79b1", 	"woyd", 	"roger75", 			1),
	("mwaters@example.com", 			"71573820cd5a24426a11f5834c152aa9", 	"szal", 	"littel.cydney", 	0),
	("qklocko@example.org", 			"255b6acdfe51828fcb230ceed819b3f6", 	"ccnm", 	"mara.ullrich", 	0),
	("stanton.stanley@example.net", 	"821c79aba4e98e72006191571f8fe077", 	"kelr", 	"ebert.kaylin", 	0),
	("cheyanne57@example.net", 			"9c2a4f404d26ce422b851d2ea87fb410", 	"zcru", 	"ayana.okuneva",	0),
	("samir.bode@example.com", 			"9e83c419b4b40b56ac971ee3fad3773f", 	"zgcl", 	"cheyanne.carter", 	0),
	("deshaun.olson@example.org", 		"4795f35574ee2b81df4439e9058de8d6", 	"yxir", 	"flo.sawayn", 		1),
	("pschinner@example.org", 			"3f573bd08564b3811ec96b35f99c8a30", 	"xmmw", 	"rsatterfield", 	0),
	("bode.joan@example.com", 			"11b4975e395e93d8140dfc334a86b007", 	"uexm", 	"devante73", 		0),
	("vcartwright@example.com", 		"073d5fedbb62d1430c5af8c28b41a453", 	"xbfm", 	"afranecki",		1),
	("lockman.eliza@example.net", 		"0c5fae7de2932c952783aa6055da49f7", 	"miwh", 	"runolfsdottir.mc", 1),
	("janice.fritsch@example.org", 		"04d0a7a67cd69b7f49fdf139c29368ba", 	"duih", 	"heathcote.amiya", 	0),
	("iprosacco@example.com", 			"bb56e26a3ac9a3faafb099c8f45d4103", 	"stou", 	"geraldine66", 		0),
	("perdman@example.com", 			"204876894f9a8e4a769b0ae80ef06856", 	"rlgf", 	"lesch.kevon", 		0),
	("marlen.collins@example.net", 		"d8d3eb031d90e96739b9730b5d12ca9e", 	"pvxp", 	"kkonopelski", 		0),
	("broderick.hills@example.com", 	"55e36e8d261c5cc3e07c3214d19d4271", 	"bzpe", 	"khalil.sipes", 	1),
	("conn.destiney@example.com", 		"a19bc511fac5af8cdfff00451c906cd4", 	"byim", 	"eden81", 			1),
	("schiller.raphael@example.net", 	"7d1c7550e602a7bc56afbe501218cf31", 	"tcnh", 	"kluettgen", 		1),
	("dorris64@example.org", 			"b127782ef8d9ec2efacf32a181160ec8", 	"xryn", 	"kelvin37", 		1),
	("beryl.funk@example.net", 			"7b485a44a23319745e63d677d7c8f617", 	"rpkd", 	"bernadine.kuhn", 	1),
	("emmerich.marie@example.net", 		"e05abc7a4e25f0dd37d6e177eb0b03f3", 	"shay", 	"marques50", 		1),
	("sonia.streich@example.com", 		"982df4a953ee030dcd6d73e848f5efb4", 	"xdpz", 	"egutmann", 		1),
	("maximus40@example.org", 			"3cfcb942cfe193f76d7b68373d8517eb", 	"mest", 	"durgan.madeline", 	0),
	("cletus.kohler@example.org", 		"068bd4bd2d95e6c6ff108bc1ec384761", 	"bzwm", 	"magali.ortiz", 	0),
	("kiel98@example.net", 				"ba2e082274e6ff5aea2b70cf0e9e407d", 	"qtzf", 	"haag.sigurd", 		0),
	("sidney.klocko@example.org", 		"f8c9dab55b7148a560c319cd2514c82a", 	"dztt", 	"thickle", 			1),
	("reese78@example.com",				"503d476e8d46a5504ce4eece1d0f5294", 	"mcnp", 	"parker.jaqueline", 0),
	("laurence.kuhlman@example.net", 	"5724c7bebd51cd392f042f0e8a0a6139", 	"qmsv", 	"brosenbaum", 		0),
	("osmith@example.net", 				"d3138fef06a66479e89dbd32943be6dc", 	"evde", 	"bernier.myra", 	1),
	("myrl.thiel@example.net", 			"31e428d718b4ff3ba87a248dff468330", 	"rsfi", 	"stark.ubaldo", 	0),
	("yazmin.ritchie@example.net", 		"60093363dd34f162cd4a283eb56996c0", 	"yvku", 	"chamill", 			0),
	("khalil25@example.com", 			"88adcc9d87f1cade0dbc9dd0d435ee9a", 	"sbgb", 	"karolann.crona", 	1),
	("homenick.francis@example.com", 	"72a81d127f35691344e8ebc1e45ffefa", 	"egcv", 	"west.baby", 		0),
	("elijah.von@example.net", 			"0d12b0b3355afe86a6c58afc469fa67c", 	"ypjb", 	"ryleigh.schultz", 	0),
	("uroob@example.com", 				"a55d37e2b478ec4dba663aabab974672", 	"tmld", 	"dietrich.brad", 	1),
	("sritchie@example.org", 			"cc21d672de50b7506d8d5b159557c583", 	"pqtp", 	"alisa.ankunding", 	0),
	("nschamberger@example.com", 		"dd79876951716dd88e83f553feaf5dec", 	"ochk", 	"weissnat.tiara", 	1),
	("lakin.vella@example.net", 		"152cedf13052a4f20cf752b7adaec790", 	"zxzv", 	"sim59", 			0),
	("cschuster@example.org", 			"dddfe569395a90a72dace61b2d5324af", 	"hopw", 	"eldora.gaylord", 	1),
	("yasmin45@example.net", 			"f16e261df951de8087a24066a4ffea15", 	"hfkz", 	"lottie71", 		1),
	("bruce.moore@example.com", 		"ba51068cd4ff9b7cbed6a3d83ab0152a", 	"peoj", 	"vivien96", 		0),
	("angelica32@example.net",			"da607b8ce19e2413cc8b5779c37d9dfd", 	"hovx", 	"omosciski", 		0),
	("precious.harvey@example.org", 	"e1f147a680f4a7165ca9286fdab965e3", 	"oipg", 	"mpowlowski", 		0),
	("moriah80@example.org", 			"7f14f5460654e7572cadf5de451df145", 	"dykb", 	"mccullough.shyan", 1),
	("kristin19@example.org", 			"ee2bfaad2066e278fd784bc947769d8b", 	"jkio", 	"jay.heller", 		1),
	("jacey97@example.org", 			"7c080a5ea38d03bab503c9f95dd7b89b", 	"yprj", 	"eveline.bartell", 	0),
	("stiedemann.kennedy@example.com", 	"9a577e0cef802c1719b7998d4b80114a", 	"topo", 	"srodriguez", 		0),
	("eve.wintheiser@example.org", 		"b8e9f2aa89bf583dce6cb28029730c56", 	"lswx", 	"hazel.koss", 		0),
	("crona.kacey@example.org", 		"745cdf6c93562c39f97e2e7c78573da7", 	"mnwn", 	"lhackett", 		1),
	("bkub@example.com", 				"691bed7336bf848db39dd3455dd1e9ca", 	"kuhf", 	"littel.teresa", 	0),
	("makayla.casper@example.com", 		"10bfdf3d5e191a9e1308b4f641b772da", 	"ypdw", 	"murphy.creola", 	1),
	("valentin75@example.org", 			"618ace4687399fca7c3c7c3043ea5bd1", 	"masm", 	"hoppe.irma", 		0),
	("neva.hilpert@example.net", 		"12b6d70bad821d59d5053f3b6860fa66", 	"sqfh", 	"helen.parisian", 	1),
	("bonnie42@example.com", 			"d344753a19f27175747a6a609108c22d", 	"devu", 	"adam47", 			0),
	("lamont59@example.org", 			"c500ee9b9a57e7bf647b616734eaef02", 	"xkbw", 	"iwelch", 			1),
	("kparker@example.org",				"89aea689c6db66f16b042ee92060df86", 	"sbkd", 	"becker.manley", 	1),
	("lhyatt@example.net", 				"9e4b378eab6c872ba2408a425ee498a0", 	"nnfm", 	"cernser", 			0),
	("kiehn.jevon@example.com", 		"1695287658637ef845c6de7dbe64e088", 	"avcg", 	"vjacobson", 		1),
	("efarrell@example.net", 			"e15dec6b98ed9cc5b4960c014756b20b", 	"hyln", 	"curtis.hintz", 	0),
	("kbraun@example.org", 				"af3c3fcc2f6b3a3bff28e32d92a7cb5c", 	"walp", 	"ehilll", 			1),
	("aschaefer@example.com", 			"b0f3c89603231363c566e70ea602f643", 	"xzcm", 	"jessie40", 		0),
	("estelle48@example.com", 			"ae0e643c3539411b5e331f1afad17057", 	"itxp", 	"lblock", 			1),
	("sarai.smith@example.net", 		"c63125ef3342462895ca2cd76471ef35", 	"uqyk", 	"kertzmann.janet", 	0),
	("jcrona@example.com", 				"534dde4ec9eb76b954c25961d0562125", 	"fauv", 	"hillary81", 		1),
	("blake31@example.org", 			"2156a41dd46bf477eaea706c0285b77e", 	"ysch", 	"fterry", 			0),
	("simonis.breana@example.com", 		"e955e50b61add5312b9ee2d42c121438", 	"icij", 	"jaskolski.gene", 	1),
	("spouros@example.org", 			"8e419db99840a8b33204c8919bd53355", 	"lkxk", 	"mlindgren", 		0),
	("mortimer31@example.com", 			"b9904f879594278cf806a541513d484f", 	"bciq", 	"hbogisich", 		0),
	("donald.bayer@example.net", 		"a6b84331b026e4fcabbff96641a416b6", 	"jpty", 	"rex42", 			1),
	("gwiza@example.net", 				"ed2bbcaa8c35d7846f1f4b8b4cf490bd", 	"kytr", 	"alfred.weimann", 	1),
	("skyla.okon@example.org", 			"e7872ba0677b7832b865e6668b19e88d", 	"meak", 	"renee.heller", 	1),
	("brett41@example.org", 			"51474f79b1b544598fcb820d55fe440f", 	"mdbx", 	"klocko.tommie", 	0),
	("king.dolly@example.net", 			"f8f64c2ea0a6b211ac537046f3f4763b", 	"fqsd", 	"griffin.welch", 	1),
	("batz.vickie@example.com", 		"93b8bf674bcbc9a62215f4929e7593c3", 	"axja", 	"cameron23", 		0),
	("jared99@example.net", 			"e3b91da3c8296fe0dd8ccc0e3b089404", 	"lsql", 	"mckayla76", 		1),
	("jswift@example.net", 				"846b9e6603a6b479a10da713f55e5b79", 	"khob", 	"hkessler", 		1),
	("schmitt.pedro@example.com", 		"854a798ab73a1676b13760c00d402c59", 	"lxqh", 	"iwalter", 			1),
	("carson02@example.net", 			"63b1cea6446d6683a97019718eefa637", 	"aqlv", 	"senger.winona", 	0),
	("brayan26@example.org", 			"aa0cfcb11a8311a019623c2f9cf6642b", 	"gebj", 	"florian58", 		1),
	("nwill@example.net", 				"aec9ebf6272b00a57fe8c53785941dd6", 	"rrny", 	"lue.towne", 		0),
	("hirthe.eden@example.org", 		"edf68bf4e611c69941d839341e34ec9b", 	"mmtq", 	"qspinka", 			1),
	("brent.toy@example.com", 			"85871a7b54e281caa4a7b24647db548c", 	"ihlk", 	"kyle68", 			1),
	("ffeest@example.com", 				"3caf0b5088234d9ec9cdcaec1169fdcf", 	"yhpl", 	"o'conner.jaron", 	0),
	("leone17@example.net", 			"e4af008512642c2c605373d456c2e9aa", 	"pqqr", 	"estrella.abshire", 0),
	("lupton@example.com", 				"e266817876fcd9b81f041b87ac1ec376", 	"hstb", 	"progahn", 			1),
	("mertz.antoinette@example.net", 	"e30b2388153f5d234f2be0f3556a6599", 	"bczk", 	"wprosacco", 		1),
	("jaiden70@example.org", 			"65c4dd36b4e0e5ee51afb3cbe206425f", 	"vqmg", 	"nelda.rowe", 		0),
	("nayeli08@example.com", 			"798b7d6ba9d418c26c98619d0b8be943", 	"qzfn", 	"xhammes", 			1),
	("buford08@example.com", 			"06e2dc75b9076913e79b4c79506ef0b5", 	"oqim", 	"jared61", 			1),
	("zvonrueden@example.org", 			"7487abb2c1e5dfaae5b72059510ac81d", 	"szzb", 	"nbergnaum", 		0),
	("gbatz@example.com", 				"551fcf24a46f059862bf7aa4fd784cda", 	"rgmz", 	"kshlerin.dessie", 	0),
	("hgaylord@example.org", 			"11bf887be5d3a7a98a697304d9b96010", 	"eogq", 	"boyle.eldred", 	1),
	("rebekah67@example.org", 			"a46a1510e979a516c3c9513576fb7927", 	"zbbi", 	"xlesch", 			0),
	("ullrich.herminia@example.net", 	"51909bf9d064966ea91e527cc0c49525", 	"tmcb", 	"ymorissette", 		1),
	("sydney.green@example.org", 		"c7b302da2093332da133279dfa9e79f5", 	"oafx", 	"juliet80", 		0),
	("elenora94@example.com", 			"ef7a1534a695aef104ed7303cdf38c48", 	"svpq", 	"colton.hauck", 	0),
	("loyce.harvey@example.net", 		"c382fe8051836ec95044dc3373a9c592", 	"etbf", 	"pacocha.roger", 	1),
	("stamm.aniya@example.net", 		"2b1a1122bd6896b07710e44f479a0358", 	"tqix", 	"eriberto36", 		1),
	("	modesto.hauck@example.org", 	"b4010c84f1c8d5fcd6a1cd8b4427532c", 	"dtap", 	"lou.von", 			0);

-- -----------------------------------------------------
-- Populate table `message`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `message` (`sender_id`, `recipient_id`, `message_content`, `date`)
VALUES 
    (57, 21, 'Dicta totam optio enim harum. Assumenda laudantium aspernatur excepturi est. Soluta quo asperiores ducimus laboriosam officia vel accusamus. Quia qui repellendus provident facilis voluptatem eveniet est.', '2020-10-13 15:07:51'),
    (21, 57, 'Et dolore eos sit voluptatem tempora tempora alias. Reprehenderit consequatur vitae vel quia. Molestiae ut numquam qui pariatur ad possimus reprehenderit. Facere dolorem quo et ut consequatur et sit. Aut ducimus dolores voluptate hic velit veniam ex.', '2020-10-14 21:00:50'),
    (57, 21, 'Nostrum illo deserunt et. Est maiores odit dolorum facilis. Aperiam sint odit in dolor ipsa omnis asperiores.', '2020-10-14 22:00:38'),
    (68, 81, 'Voluptate ad eum eaque totam et molestiae ut. Maiores neque velit ut dolore. Enim reprehenderit reiciendis hic unde at ipsam in.', '1973-09-18 23:45:33'),
    (18, 77, 'Est porro earum et alias. Facilis enim in qui consequatur. Enim numquam est ipsam veritatis et magni.', '1974-12-01 17:13:59'),
    (84, 70, 'Fugiat magni modi voluptatum. Molestiae aut incidunt placeat qui eius inventore vero possimus. Nihil quos saepe numquam culpa. Voluptates quo aut sit earum enim cumque qui.', '1999-08-17 13:59:53'),
    (10, 20, 'Ut quo aut voluptatem adipisci. Pariatur recusandae doloribus mollitia aspernatur velit. Recusandae sint provident et sint. Quibusdam laboriosam reiciendis facilis repellat consequatur magni. Ea sed qui exercitationem.', '2017-07-26 01:54:17'),
    (88, 51, 'Aut earum est aut nam placeat itaque. Eligendi quis id eveniet et illo voluptatem. Perspiciatis est deleniti et consequatur dolores quo in.', '1996-07-03 15:31:31'),
    (60, 50, 'Doloremque voluptas minus quis laudantium rerum facilis quis aspernatur. Et blanditiis nisi doloremque.', '1989-03-10 10:38:26'),
    (85, 94, 'Eligendi iure deleniti earum. Perspiciatis aut distinctio recusandae. Assumenda quia ut excepturi dolores ut hic occaecati. Est alias in perferendis totam ea voluptate et. Molestiae voluptatem vel nemo.', '1970-11-02 18:13:46'),
    (57, 37, 'Dicta totam optio enim harum. Assumenda laudantium aspernatur excepturi est. Soluta quo asperiores ducimus laboriosam officia vel accusamus. Quia qui repellendus provident facilis voluptatem eveniet est.', '1989-09-26 15:07:51'),
    (50, 96, 'Eaque sunt autem harum laudantium eum quaerat autem. Nostrum recusandae id nihil aperiam.', '1986-12-03 05:14:53'),
    (5, 83, 'Nesciunt quaerat consequatur iusto incidunt culpa dicta nemo quas. Quia eaque veniam dolor odit. Vel suscipit consequatur earum quia ipsa in sed. Quod illo architecto voluptas qui quo ipsum.', '1981-05-28 03:46:36'),
    (73, 43, 'Similique et ab qui et error quo. Dolorum unde ipsa sint tempora dolorum officiis eos. Ratione officiis modi eum aut eveniet. Molestiae eum amet neque enim.', '2013-03-29 12:28:54'),
    (52, 49, 'Soluta dolores dolorem quidem dolores dolore quasi necessitatibus aut. Delectus ut ut pariatur odio dicta nisi provident debitis. Et quos beatae reprehenderit rerum minus repellendus non.', '1984-01-29 15:27:22'),
    (36, 8, 'Nisi molestiae rerum cupiditate voluptate eos. Amet nemo enim ut expedita qui doloremque quia. Aut maiores aut cupiditate et quam ab odio accusamus. Eligendi beatae dicta atque illum.', '2000-04-18 16:15:08'),
    (46, 56, 'Molestiae soluta tempore rem aut molestiae. Et iusto alias pariatur excepturi et totam. Inventore aut occaecati in minima dolores nam. Voluptas iure repudiandae laudantium dignissimos ut.', '1990-05-12 18:25:22'),
    (64, 87, 'Minima temporibus saepe praesentium voluptatum ut. Aut veniam veritatis sit dignissimos ut tenetur. Expedita expedita assumenda error animi. Quis consectetur dicta consequatur magni asperiores. Quidem et odio cum possimus et ut asperiores.', '2003-05-06 12:21:58'),
    (22, 32, 'Cupiditate voluptatibus architecto consequatur nihil totam. Rerum similique harum facere. Eum voluptatibus sit nihil ut est eveniet necessitatibus. Voluptatem qui molestias eaque at illo laborum.', '2009-12-05 17:41:28'),
    (67, 39, 'Consequuntur tenetur iste officiis amet. Reprehenderit asperiores placeat non amet ut vel. Fuga voluptatum repudiandae necessitatibus delectus. Maiores vero temporibus blanditiis laboriosam reiciendis repellendus.', '1984-10-04 22:37:20'),
    (8, 50, 'Nulla qui consequatur culpa architecto vel soluta. Deleniti ipsam veniam delectus quia. Quia sed velit et dolores asperiores. Rerum nostrum porro nesciunt aperiam accusantium praesentium reprehenderit.', '1990-07-23 12:35:35'),
    (8, 18, 'Non voluptatem et perferendis expedita ut asperiores. Sed quia odit optio in ea similique quis. Doloremque officiis eos et quis voluptatum. Rerum aperiam culpa est aut officiis et omnis. Velit ullam earum voluptatem enim vitae omnis.', '1980-10-19 15:55:56'),
    (70, 95, 'Qui cumque minima excepturi et. Fugit consequatur amet ut accusantium odit. Rerum veniam magnam dicta voluptatem.', '1980-08-30 03:09:47'),
    (69, 30, 'Nemo et est ipsa dolorum tenetur. Tempora incidunt occaecati voluptate qui quidem aut suscipit. Quam rem quis dolores quos atque minus. Velit aut nisi nisi quis distinctio commodi. Atque aut rerum velit voluptatum est ducimus quas.', '2006-06-17 22:56:32'),
    (45, 54, 'Facere qui nobis cum aliquid. Ducimus ut sed quas animi sunt ea nam. Rerum consequatur non accusamus quae esse. Quia officiis reprehenderit magni magni eos ea.', '1989-05-11 23:49:42'),
    (23, 1, 'Dolores nobis ad doloribus cumque temporibus praesentium. Aspernatur eos quis asperiores sit voluptas quo. In iusto qui qui.', '1985-03-26 19:18:00'),
    (90, 72, 'Voluptatem voluptates modi autem id non. Quia est quod non debitis. Quae vitae reprehenderit qui aut quo eligendi. Rerum fugit voluptates et assumenda rerum magni.', '2007-11-15 14:51:29'),
    (96, 95, 'Voluptatum aut numquam voluptatem sint in velit vitae. Et corrupti consequatur omnis eligendi facere. Quas est vel magni explicabo beatae et. Illo necessitatibus modi voluptatibus reprehenderit et neque quod vel.', '2007-01-27 17:57:01'),
    (55, 68, 'Quo rerum fugit molestiae eveniet. Sint ab animi impedit dolorem qui rerum explicabo. Ullam officiis nobis aut.', '2013-06-07 11:04:39'),
    (37, 6, 'Vel ea veniam quo maxime nesciunt laboriosam asperiores. Possimus quae eos perferendis delectus corporis accusantium. Accusamus accusantium dolor qui iusto error illo sed.', '2017-03-24 12:46:16'),
    (16, 73, 'Dolorem impedit quo necessitatibus sed quos. Dolor possimus sunt sunt et aut sed dignissimos. Perferendis rerum vel aliquam laboriosam illum.', '2003-12-21 23:06:32'),
    (14, 62, 'Sed eligendi non quaerat dignissimos et. Ut soluta fugiat nesciunt et qui quia inventore suscipit. Dolores non id voluptas.', '1977-01-05 10:48:18'),
    (29, 77, 'Quia voluptate dolor voluptatem aut reiciendis. Modi omnis magnam minima qui autem occaecati commodi.', '1985-10-29 17:00:31'),
    (48, 50, 'Dicta voluptate aut error ut. Unde quisquam consequuntur est temporibus vel. Facilis tempora occaecati ut totam.', '1985-06-10 14:51:27'),
    (8, 15, 'Iure est ad non ut placeat accusamus quis. Quibusdam nostrum nulla voluptatem in ducimus saepe aliquid. Blanditiis doloremque consequatur adipisci omnis minima aperiam dolore.', '2001-06-21 18:01:40'),
    (88, 16, 'Odio ut accusantium perspiciatis occaecati dolore sit reprehenderit earum. Minima eius deserunt molestias. Et quidem voluptas sed ex maiores voluptatem quisquam. Accusantium est quasi voluptas enim.', '2012-06-15 04:54:43'),
    (65, 96, 'Voluptas quas aut fugiat sunt provident non consequatur. Quas nostrum laborum aut incidunt molestiae sit. Quaerat consectetur iste sit laboriosam doloribus. Voluptas atque est reprehenderit. Voluptas repellat aut amet similique quis.', '1997-02-23 09:28:23'),
    (34, 34, 'Repellendus deserunt voluptas itaque atque quis. Non tenetur neque quos consequatur. Provident dolorem quaerat et. Error aut voluptas sint voluptatem voluptate in vero.', '2017-05-03 15:06:02'),
    (91, 2, 'Adipisci illo quia sit quam. Vel architecto et consequatur dignissimos cumque perspiciatis deleniti. Consectetur et commodi autem autem est et sed. Rerum odit voluptatibus error neque eum corrupti.', '2010-12-16 13:36:05'),
    (63, 35, 'Voluptas et nostrum incidunt cupiditate rerum praesentium. Impedit deleniti veniam similique delectus sed. Assumenda perspiciatis sunt ratione enim.', '2007-12-25 01:29:02'),
    (55, 86, 'Fugit quae ipsam quo dolor voluptatem vel dolor et. Est nam harum quo quas sint iure. Ut nam a sint ut sint optio non. Voluptas perferendis nemo qui nobis.', '1981-02-16 00:45:49'),
    (35, 45, 'Rerum sequi eum accusamus ut vero officiis. Dicta fugit ut maxime.', '1998-02-19 21:57:05'),
    (58, 31, 'Sed possimus maxime mollitia suscipit rerum. Nulla et optio voluptatem est. Dicta omnis rerum laudantium saepe beatae dolore exercitationem consequatur. Vitae ratione ad molestiae iste est.', '2018-07-13 08:54:57'),
    (39, 12, 'Similique fugiat nobis corrupti et maxime odio reiciendis enim. Qui libero qui quas quia. Debitis nostrum aut quam inventore consectetur. Quo voluptatem et necessitatibus at id minima. Velit ex minus nam alias repudiandae nulla.', '2017-06-25 00:35:04'),
    (98, 76, 'Qui omnis non est ut. Explicabo natus accusantium eius voluptas velit. Voluptates est voluptatem est incidunt blanditiis iure voluptatum. Asperiores blanditiis suscipit laudantium molestiae quis laudantium a adipisci.', '2018-09-06 20:50:51'),
    (18, 14, 'Aut aut eos rerum voluptate mollitia laborum. Accusamus eaque qui nemo quasi.', '2007-07-15 19:27:17'),
    (48, 31, 'Nulla eum velit et non quasi molestiae. Minima nostrum omnis est dolorem ab soluta. Quis ullam debitis quae sequi expedita officiis labore.', '2015-11-08 16:43:34'),
    (75, 76, 'Non provident ipsum hic quidem. Hic minima accusantium quas consectetur. Amet ea cupiditate nulla delectus.', '2016-08-28 09:54:33'),
    (7, 22, 'Eos perspiciatis voluptate alias ea impedit autem sint. Architecto maxime harum quia aliquid qui atque. Et vero tenetur distinctio et ipsa placeat. Voluptatum aperiam ea facilis quisquam.', '1976-10-29 18:22:28'),
    (26, 15, 'Qui deleniti nam quod minus est. Sed unde et sed ea.', '2014-01-15 01:35:50'),
    (37, 14, 'Velit esse tempora sequi et iste cupiditate. Nam qui reiciendis provident alias. Et quo vel totam dolores.', '2011-08-04 11:42:59'),
    (31, 1, 'Doloribus perspiciatis ad quaerat eos. Natus odit modi et quo et repellat. Corrupti voluptatem rerum sit sit aut necessitatibus. Impedit voluptates tenetur nihil saepe reprehenderit voluptas.', '1973-08-23 05:40:18'),
    (9, 64, 'A aut non magnam beatae eligendi sit dolor. Et fugiat aliquid enim earum optio amet. Nemo pariatur consequuntur hic veniam qui. Repellendus neque perferendis itaque est. Ratione repellendus enim animi et.', '1996-06-30 17:25:25'),
    (35, 100, 'Earum vel quia minus voluptatum nemo et. Voluptas sapiente sed dolor recusandae qui omnis dolores. Natus iure fugiat distinctio sequi. Ut libero et est et.', '1985-05-26 19:40:22'),
    (66, 98, 'Iusto non ab distinctio est iste. Tempore voluptatem consectetur omnis quisquam ut. Eius reprehenderit aut voluptatem ratione consequuntur tempora eaque.', '1983-08-14 10:47:09'),
    (35, 21, 'Quo distinctio qui ex ut nihil quas nam voluptatem. Maiores voluptate aperiam et officia animi consectetur. Soluta quo quis amet sunt cupiditate consequatur labore quidem.', '1998-01-20 23:43:07'),
    (83, 70, 'Voluptatem est velit id quibusdam consectetur pariatur. Iusto rerum voluptas quibusdam placeat nobis. Rem commodi explicabo hic ut possimus. Ducimus minima consectetur et repudiandae voluptas.', '2004-02-18 01:51:23'),
    (65, 40, 'Quod aut in quia et eos ab. Quis explicabo nam dolores reiciendis. Eaque accusantium dolorem vel suscipit officiis. Debitis sunt dolore et ipsa error facere enim.', '2004-05-01 23:15:47'),
    (100, 3, 'Voluptatem temporibus repellendus provident enim quia qui asperiores error. Aut ut et voluptatem sunt officia laboriosam. Ut in officiis ab neque.', '1997-11-17 15:50:55'),
    (51, 98, 'Non voluptatem esse placeat quis. Non rerum qui voluptate hic corrupti impedit. Quidem dignissimos consectetur pariatur non voluptatem dolor quia. Eos molestiae voluptatem molestiae quas soluta.', '1976-04-10 20:50:47'),
    (78, 68, 'Qui qui distinctio tempora alias impedit. Omnis soluta veniam suscipit doloribus. Voluptatem et illo asperiores. Voluptate ipsam ipsum omnis adipisci ut.', '2012-11-10 23:32:02'),
    (11, 26, 'Sapiente enim nam dolorem. Quaerat rem hic dolorem voluptatem est non eius facilis. Dolore enim illo rem ut.', '1985-04-19 00:30:29'),
    (98, 86, 'Asperiores eius et ut deserunt nostrum adipisci quia. Repellat quia eveniet voluptatum ut velit est sint. Maiores repellendus in quo consequatur. Inventore non quo totam ea modi sunt temporibus.', '1978-04-11 10:15:29'),
    (2, 5, 'Rem consequatur iusto maxime repellendus. Omnis doloremque rem minus similique rerum doloribus quae. Hic accusantium et itaque. Eos aut impedit sunt est in eum ipsum.', '2007-09-11 08:29:40'),
    (8, 27, 'Expedita necessitatibus autem doloremque voluptatem ea. Enim deleniti placeat repudiandae commodi ratione. Quia dicta suscipit dignissimos quas molestias doloribus ratione. Quas ut rerum odio.', '2015-08-06 06:53:17'),
    (20, 44, 'Culpa quia temporibus quia dicta deleniti incidunt. Incidunt consequatur possimus libero dolores nemo. Itaque eius et rerum animi aut dolor. Temporibus sunt in praesentium exercitationem aperiam non aut non. Non reiciendis autem eos eligendi at.', '2005-12-04 15:09:56'),
    (40, 50, 'Rerum soluta omnis ad nam pariatur. Voluptatem recusandae tenetur numquam.', '2000-07-14 22:32:33'),
    (45, 3, 'Dolor et rerum numquam. Saepe animi facere nihil incidunt doloribus repellendus ratione. Ut rerum enim repellat voluptatum asperiores. Rerum deserunt dolorem ut minus dolor id. Nostrum voluptas ut possimus alias dolores voluptas adipisci.', '2015-12-02 00:58:21'),
    (14, 79, 'Velit doloribus rem praesentium omnis praesentium. Soluta cumque illum accusamus et aut ullam. Dicta et officia sed nostrum quas excepturi.', '2014-09-06 23:14:15'),
    (49, 79, 'Qui et eaque expedita vel debitis porro. Exercitationem ipsam ad at ducimus consequatur labore error. Ea rem quibusdam odit ducimus aut inventore.', '2018-10-15 16:25:23'),
    (76, 83, 'Explicabo voluptas sit et quis. Est similique accusamus omnis autem. Et ut doloribus nam soluta. Placeat ullam autem et molestiae.', '2009-09-02 23:34:45'),
    (99, 58, 'Fugit tempora mollitia suscipit dolor. Quis assumenda velit ducimus expedita voluptatem minima reprehenderit. Incidunt laudantium repellendus quae et perspiciatis corporis harum. Est asperiores nihil quod neque.', '1990-06-28 14:08:13'),
    (52, 63, 'Provident voluptatem sint ducimus porro ut voluptas. Aut perspiciatis minima consectetur est eaque aliquid. Rem omnis libero deserunt tempore facilis. Quia totam voluptatem eaque accusantium aut sequi.', '2019-04-22 18:35:00'),
    (98, 52, 'Neque non sed dolor enim quia est. Quia sed et earum sit deleniti. Ex tempora deserunt et quae. Dolorem incidunt voluptas aut possimus debitis laudantium error.', '1970-02-03 16:24:05'),
    (66, 49, 'Et consequatur voluptatem cupiditate magnam eum quas repellat. Omnis vel delectus velit fuga voluptates vel. Qui odio est eum consequuntur natus.', '2018-07-31 01:34:27'),
    (49, 44, 'Repellendus officiis eos sunt optio magnam a ipsa. Aliquam assumenda nihil expedita aliquid quas. Enim sed quo at et enim. Ea at tempore non corporis.', '1978-05-10 09:57:53'),
    (16, 60, 'Perferendis deserunt consequatur soluta ut dolores aliquam. Qui autem reprehenderit ad totam. Qui vitae est atque aut. Provident eum neque amet placeat.', '1990-01-16 12:55:03'),
    (69, 14, 'Consectetur libero dignissimos cumque harum fuga. Illum et libero blanditiis fugiat quis enim occaecati. Ducimus nesciunt natus eos id. Quam labore expedita fugit.', '1973-02-16 22:26:23'),
    (45, 71, 'Soluta provident eveniet sit aut soluta molestiae ad. Sed nobis animi deleniti nihil sed odit. Enim saepe voluptatibus rerum nihil.', '1970-06-08 08:14:28'),
    (19, 52, 'Nisi rem exercitationem voluptatem necessitatibus beatae consequatur quia. In eligendi harum saepe exercitationem officia dicta. Aliquid ex velit praesentium mollitia.', '1978-08-29 03:45:39'),
    (97, 38, 'Dolores quibusdam quam est eos. Impedit cupiditate repudiandae beatae exercitationem aut facere facere quia. Natus ab adipisci voluptate molestiae nisi.', '1981-02-15 02:32:23'),
    (96, 36, 'Dolor voluptatem enim ab vero. Ut ipsa delectus fugit sit voluptatibus illo ab. Sit necessitatibus ex ab consequatur omnis pariatur aliquam.', '2011-01-13 17:55:43'),
    (88, 40, 'Ex dolore numquam est tempora omnis eos. Aperiam et consectetur consequuntur cupiditate ea. Enim in deleniti qui consequuntur asperiores provident ut.', '2016-01-22 13:09:52'),
    (85, 1, 'Porro tempora deserunt rerum non consectetur sed. Et culpa quisquam dolores officia. Voluptatem laborum voluptates qui assumenda dolorem est voluptatibus.', '1974-04-25 11:05:49'),
    (18, 33, 'Aut autem facere et eius occaecati rerum molestiae ratione. Culpa quo sed harum at delectus rerum. Corporis rerum et enim ut voluptatem. Porro deleniti in error qui minus totam facere.', '1975-07-21 02:03:26'),
    (80, 94, 'Dolorem blanditiis modi corrupti impedit et illum eveniet. Nobis nihil voluptas omnis et.', '1996-04-06 13:20:18'),
    (15, 78, 'Hic repellendus occaecati nesciunt repellat rem tempore. Fugiat facere vel ducimus et ut consequatur qui rerum. Aliquid et aut error dolore. Voluptates numquam aut iste asperiores ab reiciendis dolores.', '1990-12-11 07:41:47'),
    (52, 67, 'Velit enim laboriosam ut libero. Exercitationem voluptas earum sunt voluptatem commodi at harum. Assumenda fugiat natus dicta eius. Molestiae nihil odio expedita quo ut est rerum.', '1989-07-27 00:12:33'),
    (41, 50, 'Voluptas dolore sint maxime et vel. Quidem aut iste aut possimus itaque explicabo odio. Ut modi ea vero et ut neque.', '1973-04-22 12:09:17'),
    (18, 7, 'Provident et sed est sed. Distinctio ab maiores reprehenderit quo magnam dicta porro. Vel natus fugit ut architecto facilis praesentium voluptas.', '2007-10-08 14:40:43'),
    (98, 67, 'Ipsum qui quam enim quasi et iure libero. Harum libero est sit. Adipisci quia assumenda consequatur ea accusantium atque quia. Eum eos et tempora vel et.', '1975-07-03 01:29:10'),
    (51, 14, 'Deleniti voluptas ea est dolores. Quos architecto fugit est consequuntur molestiae repudiandae. Esse consequatur excepturi id nemo provident. Libero fuga et rerum velit voluptatem aperiam.', '1975-06-17 02:50:36'),
    (26, 20, 'Distinctio natus commodi iusto quasi sed qui omnis. Quisquam beatae ratione quibusdam ea. Quia dolores esse sed iste. Sit placeat laboriosam quam asperiores excepturi voluptas iusto perspiciatis.', '2008-10-31 13:15:45'),
    (27, 71, 'Voluptatem error ducimus sed eligendi voluptas qui. Iste voluptatem nulla aliquam et dolore iusto facere. At laborum modi dolore minima earum error. Dolores enim omnis ab excepturi.', '2004-09-20 22:08:29'),
    (90, 46, 'Quia modi quibusdam hic sapiente maiores placeat. Magnam velit possimus aspernatur molestiae fuga. Ut aut corporis consectetur nihil hic esse consequatur. Consequatur maiores placeat praesentium vitae aut.', '1975-05-30 15:53:58'),
    (22, 86, 'Doloribus similique voluptatum cumque impedit blanditiis culpa. Aliquid eveniet est et voluptatem eaque maxime. Perferendis molestiae perferendis recusandae dicta delectus eius mollitia. Dicta laboriosam cumque est vitae voluptate debitis.', '2009-01-21 13:45:45'),
    (83, 17, 'Ut reprehenderit non illo fuga ut officia repudiandae ducimus. Rerum ex qui ex qui consequatur et. Velit tempore illo voluptates sed. Magni rerum aut ut placeat tempora est hic minima.', '1973-01-22 11:45:07'),
    (22, 71, 'Et qui aut optio placeat natus et est. Sapiente qui at sit consectetur.', '1996-04-22 04:31:33'),
    (57, 6, 'Quas praesentium voluptatum quia explicabo laudantium error. Soluta ea quis iure sed dolorem et quia distinctio. Quod facilis cumque voluptas aliquam.', '1993-03-04 18:26:15'),
    (72, 2, 'Suscipit eaque qui nulla ut quia aut et. Dolor incidunt eius velit culpa dolores consequatur sit. Vero fuga ea laborum quaerat amet labore veritatis exercitationem. In inventore nesciunt doloremque in illo neque.', '1997-02-12 23:38:37');


-- -----------------------------------------------------
-- Populate table `host_place`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `host_place` (`adress`, `zipcode`, `city`, `country`, `capacity`, `description`, `type`, `host_id`) 
VALUES 
  ('13465 Green Bypass Apt. 475', '76369-8607', 'Batzmouth', 'Oklahoma', '4', 'A velit vel cum doloribus. Et voluptate et qui omnis illum blanditiis esse. Fugit officia reiciendis quaerat tempora. Dolorem similique distinctio rem labore ipsa odio repellendus.', 'dortoir', 2),
  ('650 Lubowitz Parks', '41734', 'East Benedictfort', 'Virginia', '3', 'Doloremque quia ut occaecati. Praesentium ut blanditiis porro repudiandae. Asperiores vel qui asperiores reiciendis laudantium. Iusto in et quia eos omnis dolor dignissimos possimus.', 'dortoir', 8),
  ('1439 Hosea Heights Apt. 247', '13097', 'Daphnetown', 'Wyoming', '8', 'Pariatur quae dolor incidunt aperiam nihil est eos. Ex nobis laudantium nisi perferendis labore neque iusto. Doloremque dignissimos cumque nostrum vel dolorem.', 'dortoir', 11),
  ('66525 Laila Coves Suite 176', '11074-0895', 'Johnsmouth', 'Utah', '8', 'Esse iste qui veritatis molestiae tempora quae eum odit. Laboriosam dolorum facilis eaque mollitia et. Labore eveniet similique est nulla tempore autem sint. Consectetur praesentium qui sapiente minima. Sed dignissimos voluptas sit.', 'tentes', 12),
  ('50734 Abraham Haven Apt. 457', '25794-7043', 'East Mikeport', 'Massachusetts', '10', 'Ducimus voluptas magnam labore optio molestiae qui doloribus. Ipsa et dolorem ea enim amet molestias. Ut sed officiis minus. Sapiente accusantium omnis repellendus fuga aut voluptatem.', 'chambre individuelle', 17),
  ('71360 Cecil Green', '18835', 'Heidenreichfurt', 'Arizona', '7', 'Quibusdam laborum id illum eaque aut delectus. Perspiciatis nam et perferendis illum ducimus ea. Temporibus molestias quo architecto architecto ullam. Eveniet vel sequi nostrum debitis quaerat sit.', 'dortoir', 18),
  ('768 Ernser Light Apt. 327', '16893-4187', 'Port Adrienne', 'Missouri', '', 'Fugit porro molestiae error rerum corrupti nam dolorum sed. Accusantium eligendi possimus animi omnis quia beatae. Ullam atque ut dignissimos excepturi sint quibusdam. Rerum ratione architecto corrupti quis corporis minima.', 'tentes', 19),
  ('4702 Hills Hill Suite 128', '37449-2195', 'Arelymouth', 'Illinois', '4', 'Sit magnam consectetur laborum et vero voluptatem. Saepe architecto laboriosam velit eum possimus fuga tempore qui. Tenetur et voluptas exercitationem. Et quod quibusdam quis voluptas dolore consequatur.', 'chambre individuelle', 20),
  ('19853 Clara Camp Apt. 214', '01913-4096', 'Birdiechester', 'SouthCarolina', '9', 'Fugiat ex dolor nisi. Alias molestiae aperiam ratione vitae ut non est ab. Deserunt eligendi voluptas et.', 'tentes', 21),
  ('4789 Quigley Greens Apt. 394', '61779-9703', 'Armstrongmouth', 'Nebraska', '4', 'Quo odit unde totam error itaque. Itaque aut incidunt architecto deserunt qui. Est ut exercitationem provident tempore possimus. Ad est repellat maiores ut nemo sed. Architecto voluptatem veniam quo labore amet perspiciatis.', 'dortoir', 22),
  ('559 Durgan Overpass Suite 830', '21844-1662', 'North Mariano', 'NewYork', '3', 'Assumenda accusantium ut rerum dolores. Error id voluptatem architecto qui possimus. Et dolorem non rerum tempore explicabo veritatis.', 'chambre individuelle', 23),
  ('3228 Annetta Crossroad Suite 908', '43159-0328', 'North Cielo', 'Wyoming', '10', 'Eligendi eos doloribus ducimus quis. Et corrupti eos aut vel quasi aut. Aut ipsa voluptas facilis sit enim. Officiis impedit voluptates sit cum aspernatur sint.', 'dortoir', 27),
  ('966 Senger Common', '11903', 'Jovanifort', 'Virginia', '2', 'Molestias aut mollitia sit dolores temporibus doloremque. Porro tempora dicta ipsam quo nemo explicabo. Aut magni voluptatibus maiores consequatur.', 'tentes', 30),
  ('8848 Orie Prairie', '84143-5497', 'Laurenview', 'Colorado', '3', 'Nulla dolorem sint tempora aut. Illo eligendi rem officiis.', 'dortoir', 33),
  ('339 Cormier Circles', '70108-2859', 'West Keyshawnborough', 'Oklahoma', '5', 'Sed repellat alias harum ducimus distinctio dignissimos qui. Id voluptatem non distinctio ut commodi dolores veniam. Consequatur consectetur et qui. Sed in possimus blanditiis esse molestiae.', 'tentes', 36),
  ('079 Edison Isle Apt. 108', '77849', 'Lake Jacinto', 'Idaho', '9', 'Atque et rerum et. Non sed explicabo consequatur quia placeat exercitationem tempore. Laboriosam et culpa id ut commodi quia quaerat. Soluta sed soluta repudiandae cum quis molestiae.', 'dortoir', 38),
  ('940 Jason Glen', '77809-8020', 'Lomabury', 'Colorado', '4', 'Odio dolores suscipit voluptas incidunt voluptas recusandae sunt. Sint tempora veniam occaecati facere consequatur est. Dolor est voluptas iusto ullam et aliquid officia.', 'tentes', 40),
  ('346 Sporer Camp Apt. 750', '92061', 'Hintztown', 'Minnesota', '10', 'Earum quo nulla pariatur recusandae vitae tempore sequi nobis. Ab dolore laboriosam quaerat. Ad sunt quia autem placeat voluptatem velit et. Facere quaerat mollitia nobis et qui.', 'tentes', 41),
  ('763 Ruby Cliffs Suite 456', '45109-5094', 'New Hortensefurt', 'Delaware', '3', 'Voluptatem occaecati non aut nam ipsa excepturi sint. Dolorem est cupiditate aut reprehenderit. Quisquam eveniet ipsum beatae sit itaque quaerat totam. Adipisci enim doloremque eum excepturi beatae nostrum aspernatur doloremque.', 'tentes', 45),
  ('316 Rene Views Suite 736', '71181', 'Theotown', 'Maine', '8', 'Exercitationem voluptatibus libero ipsa earum. Et fugiat maxime neque enim ratione culpa fugiat unde. Reprehenderit et et necessitatibus voluptas. Nihil cum possimus suscipit quas.', 'dortoir', 46),
  ('7209 Krajcik Dale', '40478', 'Damarischester', 'Vermont', '', 'Nostrum nesciunt quia vel. Et voluptate vel eaque amet. Tempore debitis est iste id voluptatibus architecto delectus.', 'chambre individuelle', 50),
  ('02127 Ratke Ranch', '91647', 'Fredericbury', 'Oklahoma', '3', 'Laboriosam eligendi est rerum natus. Qui recusandae aut deleniti consectetur. Omnis corrupti qui ea delectus modi qui est. Ipsum suscipit ut inventore eaque aut dolores nesciunt est.', 'dortoir', 52),
  ('890 Rafaela Trafficway Suite 242', '50893-7990', 'West Autumnborough', 'Tennessee', '3', 'Qui modi vel omnis et provident facere quod. Nihil soluta corrupti ab rem. Ipsam consectetur fugiat omnis rem minima.', 'tentes', 54),
  ('2344 Era Rue', '20140', 'Toyfort', 'Arkansas', '8', 'Fugiat maiores quas dignissimos expedita facilis voluptatem sit sint. Dolor vero vel eos doloremque sunt. At nemo inventore qui eligendi. Inventore et sint quis numquam magni voluptates similique et.', 'chambre individuelle', 56),
  ('99364 Thiel Shores', '31223', 'Hintzshire', 'Idaho', '1', 'Non dolore qui sit corporis. Possimus illum cum aut doloribus sed. Error dolores sint nihil eius ut cumque.', 'dortoir', 57),
  ('63765 Lockman Ramp Apt. 791', '10269', 'Jacklynshire', 'NewMexico', '', 'Corporis at sint omnis nesciunt nam vel. Velit accusamus voluptatum placeat sequi. Tempora hic minus aliquid nulla libero tempore.', 'dortoir', 59),
  ('458 Aubrey Route Suite 694', '50676-2783', 'Kertzmanntown', 'Minnesota', '2', 'Eveniet totam consequatur eaque quisquam similique quibusdam ipsa. Odit repellat repudiandae quidem est aut qui sequi. Omnis omnis voluptatem doloribus cum officia ea.', 'dortoir', 61),
  ('0128 Willms Branch Suite 468', '82520', 'North Buster', 'Alaska', '10', 'Deserunt cum ut ipsam illum deserunt quaerat omnis. Sint at asperiores quia id. Exercitationem explicabo qui temporibus qui accusantium qui fuga. Quia cumque magni ut exercitationem et delectus sunt. Illo voluptatem voluptatum consequatur et nam dolorum molestiae.', 'chambre individuelle', 63),
  ('394 Cristobal Ford Apt. 008', '94067', 'New Aniyahhaven', 'Nevada', '8', 'Voluptatem quia harum omnis dolor distinctio aliquid. Autem nisi et quasi facilis cumque sint et ipsa. Ut ducimus quia est. Voluptatum ex et laborum animi corrupti.', 'tentes', 65),
  ('56734 Lou Tunnel Suite 019', '94101-4664', 'South Madonnabury', 'Kansas', '4', 'Illum facere vel nemo veniam at ut quasi veritatis. Fuga quis voluptas minus repellendus iure. Aperiam non nesciunt odit reiciendis.', 'chambre individuelle', 67),
  ('2615 Columbus Parks Suite 097', '41458-2083', 'Gleasonfort', 'NewJersey', '4', 'Omnis eveniet est odit tempore sint. Rerum voluptas non qui iure dolores repellat. Fugit expedita harum corrupti omnis omnis quaerat. Ut voluptas inventore rem qui est non quia.', 'chambre individuelle', 70),
  ('4636 Stephen Village', '27176', 'Clintonstad', 'Alabama', '9', 'Nemo voluptatem exercitationem iure est hic omnis. Sunt quia voluptas unde corporis. Reiciendis placeat et qui molestiae voluptatem et.', 'chambre individuelle', 71),
  ('238 Bins Village', '89165', 'Opheliaside', 'Hawaii', '9', 'Asperiores exercitationem repellat et voluptates. Velit itaque in rerum reiciendis. Incidunt alias tempora est est magni et blanditiis.', 'dortoir', 72),
  ('1484 Frank Cliff Apt. 203', '74134-4561', 'West Devinshire', 'Connecticut', '', 'Voluptate laborum cupiditate amet non omnis soluta qui. Sed eveniet aut beatae rerum. Quos dolores a error.', 'tentes', 74),
  ('258 Tad Fords', '51323-8598', 'Greenfelderview', 'NewMexico', '10', 'Nihil vel dolor esse. Qui optio at quia voluptatem quaerat aut. Dicta sed modi qui facilis voluptatem qui quis. Sint rerum quia omnis.', 'chambre individuelle', 76),
  ('13955 Konopelski Mills', '39356', 'East Jolie', 'Maryland', '10', 'Molestias aut velit quae. Iste iste vel enim qui. Modi et perferendis autem ea distinctio voluptatem. Et delectus qui nobis aut dicta aut.', 'dortoir', 77),
  ('6873 Shanon Extension', '83932', 'West Sim', 'Oregon', '10', 'Dignissimos reprehenderit ut doloribus quaerat quidem dicta et. Voluptatum labore inventore sed vel. Ipsam eos nulla minus nam. Repellendus vitae eos quia enim molestias perspiciatis ut.', 'tentes', 78),
  ('546 Luna Locks Suite 785', '88339', 'East Abdielton', 'Arkansas', '10', 'Aut ea tempora vitae et molestiae dolorum. Voluptatem possimus quae eius rem. Quia voluptatum doloribus vitae quidem voluptates qui debitis. Rerum dolor et dolorem eos dolor quod.', 'chambre individuelle', 80),
  ('930 Luettgen Camp', '10307-6125', 'North Delfinaton', 'Maine', '8', 'Voluptatem voluptatem asperiores est qui optio molestiae. Et rerum non voluptatem sunt blanditiis. Eos magnam sed reiciendis vero labore fugit officiis.', 'chambre individuelle', 82),
  ('34697 Reynolds Ramp Suite 005', '01141-2650', 'Imogeneville', 'NorthDakota', '7', 'Sed vero qui repellendus voluptas laboriosam fugiat at a. Alias rerum vel perferendis ipsam voluptatibus. Voluptatem libero necessitatibus labore voluptatem tempora aut a vitae.', 'dortoir', 83),
  ('5231 Zieme Route', '20465-3614', 'Adrianborough', 'Minnesota', '2', 'Odit in voluptatem doloremque repellat et est. Odit qui consequuntur dolor ipsam molestiae doloribus. Consequatur assumenda perferendis consequatur aut corporis.', 'chambre individuelle', 86),
  ('8453 Ocie Loop', '73280-9868', 'Robertsport', 'Tennessee', '7', 'Laboriosam vel numquam repudiandae ad. Sunt aperiam architecto ratione doloremque culpa ut unde qui. Reprehenderit qui laboriosam sit quia. Totam sequi aliquid alias distinctio id excepturi.', 'dortoir', 87),
  ('1540 Davis Curve Apt. 648', '61925-7769', 'McKenziebury', 'NewMexico', '5', 'Similique saepe qui illum consequatur. Blanditiis magnam autem beatae soluta iure voluptatum. Ut sint accusantium occaecati laboriosam porro nihil odio sed.', 'chambre individuelle', 89),
  ('595 Wilburn Fall', '57207', 'Arvelchester', 'Illinois', '9', 'Consectetur ad aut pariatur sed est est aut alias. Dolore blanditiis animi aut nesciunt non nemo. Sed consequatur nisi temporibus. Qui quo consequatur iusto consequatur.', 'tentes', 90),
  ('72482 Schoen Estates Suite 003', '06256-3516', 'Ornmouth', 'SouthCarolina', '', 'Eum ut amet sunt velit quia sit. Necessitatibus et sunt sint ipsam sunt libero. Quia eum nulla esse doloremque asperiores necessitatibus totam porro.', 'tentes', 93),
  ('79941 Emilia Circles Suite 232', '09290', 'Port Darwinport', 'Pennsylvania', '7', 'Eveniet dolorem velit fugiat consequatur ullam quod ut. Sit provident placeat omnis illo expedita eligendi nihil. Ut distinctio aspernatur totam temporibus aperiam dolores.', 'dortoir', 95),
  ('882 Dooley Gardens', '86025-6234', 'West Feltonfurt', 'Minnesota', '8', 'Esse esse rerum hic enim. Et at iste quo ab harum. Expedita molestiae cumque et tempora. Vitae unde quisquam autem libero perferendis harum rem incidunt.', 'tentes', 98),
  ('3772 Vandervort Island', '99735', 'East Bellview', 'Oregon', '10', 'Quasi amet quia dignissimos excepturi autem quasi culpa. Sunt sit illum omnis iure rerum. Eligendi omnis ea aut rem.', 'dortoir', 99);


-- -----------------------------------------------------
-- Populate table `project`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `project` (`from_date`, `to_date`, `host_place_id`) 
VALUES
    ('1999-06-03', '1999-07-03', 1),
    ('1989-11-25', '1989-12-25', 2),
    ('2001-04-21', '2001-05-21', 3),
    ('1980-02-14', '1980-03-14', 4),
    ('1997-06-10', '1997-07-10', 5),
    ('1972-10-10', '1972-12-10', 6),
    ('1978-02-24', '1978-04-24', 7),
    ('2020-10-16', '2020-12-16', 7),
    ('1982-04-16', '1982-06-16', 8),
    ('1978-05-04', '1978-06-04', 9);


-- -----------------------------------------------------
-- Populate table `stay`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `stay` (`from_date`, `to_date`, `arrival`, `departure`, `volonteer_id`, `project_id`, `host_place_id`) 
VALUES
    ('1999-06-03', '1999-07-03', null, null, 51, 1, null),
    ('1989-11-25', '1989-12-25', '1989-11-26', '1989-12-25', 59, 2, null),
    ('2001-04-21', '2001-05-21', '2001-04-21', '2001-05-19', 22, 3, null),
    ('1980-02-18', '1980-03-01', '1980-02-18', '1980-03-01', 80, 4, null),
    ('1980-02-14', '1980-02-24', '1980-02-14', '1980-02-24', 10, 4, null),
    ('1980-03-01', '1980-03-14', '1980-03-01', '1980-03-14', 32, 4, null),
    ('1997-06-10', '1997-07-10', '1997-06-10', '1997-07-10', 58, 5, null),
    ('1978-02-24', '1978-04-24', '1978-02-24', '1978-04-24', 20, 7, null),
    ('2020-10-16', '2020-12-16', '2020-10-16', '2020-12-16', 4, 8, null),
    ('1982-04-16', '1982-06-16', '1982-04-16', '1982-06-16', 13, 9, null),
    ('2017-03-12', '2017-04-06', '2017-03-12', '2017-04-06', 29, null, 39),
    ('1998-10-28', '1998-11-28', '1998-10-28', '1998-11-28', 62, null, 25),
    ('1975-08-13', '1975-09-13', '1975-08-13', '1975-09-13', 99, null, 33),
    ('1998-08-18', '1998-08-29', '1998-08-18', '1998-08-29', 65, null, 32),
    ('1998-04-29', '1998-05-15', '1998-04-29', '1998-05-15', 82, null, 39),
    ('2018-03-10', '2018-03-25', '2018-03-10', '2018-03-25', 65, null, 7),
    ('1994-07-05', '1994-07-31', '1994-07-05', '1994-07-31', 96, null, 24),
    ('1992-01-03', '1992-01-12', '1992-01-03', '1992-01-12', 31, null, 40),
    ('1990-01-15', '1990-01-31', '1990-01-15', '1990-01-31', 60, null, 25),
    ('2000-07-10', '2000-07-23', '2000-07-10', '2000-07-23', 47, null, 32),
    ('1988-02-19', '1988-02-15', '1988-02-19', '1988-02-15', 8, null, 4);


-- -----------------------------------------------------
-- Populate table `skill`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `skill` (`name`, `description`) 
VALUES
    ('iste', 'Neque possimus vero est cumque minus tempore perferendis. Vero ut deserunt et vitae qui. Sint quasi itaque cum omnis doloremque ut neque quam.'),
    ('perferendis', 'Facere molestiae et doloremque atque quo earum inventore. Sunt eligendi laboriosam autem vero aut voluptas. Officiis ex saepe omnis accusamus fugit voluptatem exercitationem placeat.'),
    ('harum', 'Ut accusamus temporibus eos hic distinctio error et. Nobis ad dolorem occaecati omnis enim. Dolor ipsum exercitationem in earum est aliquam.'),
    ('modi', 'Aliquid soluta aut dolores unde est ea. Dolorem ab eius molestiae maxime nulla.'),
    ('dolore', 'Blanditiis vel aperiam iusto ipsam. Quod nemo et id sapiente corrupti.'),
    ('quod', 'Id porro corporis nulla quia vitae quis doloremque. Accusamus laudantium quia deserunt error veritatis nulla. Consequatur iste molestias excepturi animi. Natus dolor qui culpa velit. Earum consequatur culpa sit quia.'),
    ('qui', 'Sunt ea aut dicta sunt sunt. Optio voluptas ea ut aut dolorum omnis architecto.'),
    ('eum', 'Asperiores accusamus maxime esse quos. Assumenda et velit ex. Maxime quae sint dolorem inventore ut at.'),
    ('doloribus', 'Excepturi sunt est blanditiis accusantium. Quisquam id nam dolore cumque sed et aut. Voluptates facilis at delectus assumenda beatae iusto. Itaque molestias aut dolorem.'),
    ('et', 'Magnam itaque eum fuga quibusdam odio voluptas voluptatem. Amet consequatur magnam et asperiores facere voluptatem sit. Maiores asperiores aliquid quod velit cumque velit saepe.'),
    ('sit', 'Ratione est enim fugiat eaque doloremque. Non ex impedit rerum quaerat laboriosam earum facere tempora.'),
    ('at', 'Quo itaque molestias et amet eligendi et natus id. Dignissimos quisquam expedita ipsa. Odio perferendis quo tempora minus. Autem eum ut aut sit sunt quis enim.'),
    ('minima', 'Libero et natus fugit voluptate consequatur. Et soluta eligendi libero. Animi in quis dolor nisi.'),
    ('est', 'Alias velit ipsa culpa ut. Est aut ullam sint voluptatibus minima.'),
    ('et', 'Adipisci id quae cum sapiente. Ducimus at ut sit dolor eius omnis omnis. Fugit ut repellendus incidunt quo rem deleniti neque.'),
    ('minima', 'Nihil ut vel labore omnis sit quibusdam. Iure ipsa quod earum dolorem quia aut. Fuga incidunt et consequatur nisi omnis.'),
    ('velit', 'Nesciunt cum aut similique. Occaecati hic expedita placeat non blanditiis ut. Ut facilis numquam minus nemo.'),
    ('saepe', 'Animi et quisquam accusantium reprehenderit aspernatur consectetur atque. Natus magnam sit pariatur quaerat aut. Omnis qui et ab mollitia doloribus.'),
    ('non', 'Nam reprehenderit sed sint sit sit eligendi. Neque rem dolore atque aut sed temporibus. Blanditiis officia in velit autem animi.'),
    ('praesentium', 'Sapiente error aut in voluptates inventore nisi. Eum consequatur voluptas assumenda voluptas quia. Eaque rerum a et cum et. Nostrum recusandae quas cum quia distinctio blanditiis.');


-- -----------------------------------------------------
-- Populate table `host_place_has_skill`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `host_place_has_skill` (`host_place_id`, `skill_id`)
VALUES
    
    (34,7),
    (45,20),
    (25,4),
    (2,7),
    (45,1),
    (18,18),
    (17,10),
    (32,6),
    (41,16),
    (1,10),
    (37,14),
    (22,10),
    (48,11),
    (19,2),
    (30,10),
    (2,17),
    (45,19),
    (24,8),
    (19,12),
    (18,16),
    (1,12),
    (2,17),
    (39,19),
    (44,7),
    (15,3),
    (14,3),
    (9,11),
    (11,11),
    (4,13),
    (26,2),
    (33,10),
    (18,5),
    (5,16),
    (24,4),
    (29,1),
    (5,17),
    (6,12),
    (19,7),
    (11,6),
    (15,4),
    (5,10),
    (17,20),
    (23,19),
    (44,11),
    (24,8),
    (9,9),
    (25,5),
    (39,11),
    (15,11),
    (14,8),
    (16,20),
    (23,7),
    (30,5),
    (10,4),
    (34,5),
    (6,7),
    (13,12),
    (27,5),
    (30,9),
    (4,16),
    (36,3),
    (47,10),
    (41,5),
    (42,16),
    (40,14),
    (44,2),
    (39,3),
    (20,18),
    (28,9),
    (12,10),
    (20,6),
    (38,11),
    (10,13),
    (38,13),
    (41,6),
    (2,11),
    (26,13),
    (9,16),
    (27,12),
    (24,19),
    (6,7),
    (12,6),
    (11,14),
    (12,13),
    (27,15),
    (2,20),
    (40,11),
    (39,8),
    (32,3),
    (28,13),
    (36,20),
    (44,2),
    (15,1),
    (6,15),
    (40,8),
    (21,20),
    (19,2),
    (24,20),
    (38,16),
    (33,14);


-- -----------------------------------------------------
-- Populate table `project_has_skill`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `project_has_skill` (`project_id`, `skill_id`) 
VALUES
    (8,10),
    (10,17),
    (1,4),
    (3,15),
    (1,12),
    (4,10),
    (3,16),
    (3,6),
    (9,1),
    (8,5),
    (10,14),
    (6,10),
    (2,14),
    (3,18),
    (4,4),
    (6,11),
    (1,9),
    (8,9),
    (2,9),
    (1,7);


-- -----------------------------------------------------
-- Populate table `user_has_skill`
-- -----------------------------------------------------
USE `woofing`;
INSERT INTO `user_has_skill` (`user_id`, `skill_id`, `level`) 
VALUES 
    (11,7,4),
    (76,17,3),
    (45,16,5),
    (1,1,3),
    (36,15,3),
    (40,14,4),
    (41,6,5),
    (81,11,5),
    (53,17,3),
    (33,9,3),
    (82,2,1),
    (54,19,5),
    (7,7,1),
    (89,19,5),
    (69,8,1),
    (23,12,3),
    (59,1,3),
    (26,9,1),
    (61,14,2),
    (67,12,3),
    (31,10,3),
    (49,9,4),
    (21,5,4),
    (69,16,3),
    (12,9,2),
    (79,11,3),
    (27,2,4),
    (53,7,1),
    (9,2,3),
    (39,3,1),
    (89,10,1),
    (52,6,1),
    (45,5,2),
    (18,20,4),
    (53,6,3),
    (93,17,3),
    (86,6,2),
    (16,15,4),
    (38,11,1),
    (2,12,3),
    (15,8,4),
    (44,19,5),
    (19,14,2),
    (4,11,4),
    (65,20,1),
    (95,12,1),
    (37,13,2),
    (66,5,3),
    (1,19,1),
    (73,9,1),
    (59,3,1),
    (93,2,1),
    (26,4,2),
    (9,16,3),
    (7,17,1),
    (92,15,2),
    (1,14,3),
    (35,3,4),
    (17,1,5),
    (68,1,5),
    (84,2,3),
    (35,4,4),
    (61,1,4),
    (71,1,1),
    (40,11,3),
    (38,1,2),
    (87,1,1),
    (53,1,3),
    (50,1,2),
    (66,11,4),
    (36,13,1),
    (20,8,2),
    (100,2,3),
    (89,13,5),
    (51,2,4),
    (90,14,3),
    (68,7,2),
    (82,7,5),
    (41,2,5),
    (11,19,2),
    (13,16,2),
    (84,6,5),
    (21,18,2),
    (32,10,1),
    (40,17,4),
    (42,19,3),
    (46,15,5),
    (49,2,1),
    (17,18,1),
    (8,12,1),
    (16,6,4),
    (96,12,1),
    (38,13,4),
    (14,9,2),
    (33,19,4),
    (73,11,1),
    (71,15,1),
    (45,14,5),
    (90,15,5),
    (74,10,5);
